import React, { useState, useEffect } from 'react'
import { useParams } from 'react-router-dom'
import { useSelector } from 'react-redux'

import Card from '../../components/Card'
import HeaderPage from '../../components/HeaderPage'

import { sendRequest } from '../../helpers/sendRequest'
import { API_URL } from '../../config/API'

import './PostComments.scss'

const PostComments = () => {
  const [comments, setComments] = useState([])
  const entity = 'comments'
  const { PostID } = useParams()

  useEffect(() => {
    sendRequest(`${API_URL}/posts/${PostID}/${entity}`).then((data) => {
      setComments(data)
    })
  }, [PostID])

  const postName = useSelector((state) => {
    const { postNameReducer, userNameReducer } = state
    return `Post Comments - ${userNameReducer.userName} - ${postNameReducer.postName}`
  })

  return (
    <>
      <HeaderPage title={postName} />
      <div className="comments__dashboard">
        {comments &&
          comments.map(({ name, body, email }, key) => (
            <Card
              commentTitle={name}
              commentBody={body}
              commentEmail={email}
              key={key}
            />
          ))}
      </div>
    </>
  )
}

export default PostComments
