import React from 'react'

import { ReactComponent as StarSvg } from '../../img/star.svg'
import ButtonComponent from '../ButtonComponent/ButtonComponent'

const ProductCard = ({
  item,
  item: { articul, name, price, image, color },
  isFavourite,
  onClickBtn,
  onClickStar,
  btnContent: { backGroundColor, title },
}) => {
  const clickBtn = () => {
    onClickBtn(item, `${name} (${color})`)
  }

  const clickStar = () => {
    onClickStar(item)
  }

  return (
    <div id={articul} className="item">
      <div className="item__image">
        <img src={image} alt={name} width="350px" height="350px" />
      </div>
      <div className="item__info">
        <h2>
          {name} ({color})
        </h2>
        <p>{price}</p>
        <span onClick={clickStar}>
          {isFavourite ? <StarSvg fill="green" /> : <StarSvg />}
        </span>
      </div>
      <div className="item__buttons">
        <ButtonComponent
          backGround={backGroundColor}
          title={title}
          onClick={clickBtn}
        />
      </div>
    </div>
  )
}

// ProductCard.propTypes = {
//   item: PropTypes.shape({
//     articul: PropTypes.string.isRequired,
//     name: PropTypes.string.isRequired,
//     price: PropTypes.string.isRequired,
//     image: PropTypes.string.isRequired,
//     color: PropTypes.string.isRequired,
//   }).isRequired,
//   isFavorite: PropTypes.bool.isRequired,
//   generateModalForBtn: PropTypes.func.isRequired,
//   onClickStar: PropTypes.func.isRequired,
// }

export default ProductCard
