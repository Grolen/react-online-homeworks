import React, { Component } from 'react'
import './Modal.scss'

export default class Modal extends Component {
  closeHandler = () => {
    this.props.onClose()
  }

  render() {
    const { modalObj } = this.props
    const { id, header, closeButton, text, actions } = modalObj
    return (
      <>
        <div className={`modal ${id}`}>
          <div className="modal__header">
            <h2 className="modal__header__title">{header}</h2>
            {closeButton && (
              <button
                className="modal__header__btn"
                onClick={this.closeHandler}
              >
                &times;
              </button>
            )}
          </div>
          <div className="modal__content">
            <p className="modal__content__text">{text}</p>
            {actions}
          </div>
        </div>
        <div className="backdrop" onClick={this.closeHandler} />
      </>
    )
  }
}
