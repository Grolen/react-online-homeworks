import React from 'react'
import cx from 'classnames'
import { Link } from 'react-router-dom'

import { ReactComponent as EditIcon } from './images/edit.svg'

const Card = (props) => {
  const {
    email,
    name,
    username,
    phone,
    city,
    street,
    companyName,
    catchPhrase,
    children,
    className,
    postTitle,
    postBody,
    postId,
    albumTitle,
    albumBody,
    albumId,
    photoTitle,
    photoBody,
    commentTitle,
    commentBody,
    commentEmail,
    onClick,
  } = props

  return (
    <div className={cx('card', className)}>
      <div className="card-box">
        <div className="card-header">
          {postTitle && <p className="card-title">{postTitle}</p>}
          {postTitle && (
            <div className="card-edit">
              <Link to={`${postId}/edit`}>
                <EditIcon width="20px" height="20px" />
              </Link>
            </div>
          )}
          {commentTitle && <p className="card-title">{commentTitle}</p>}
          {albumBody && <p className="card-title">{albumBody}</p>}
          {photoBody && <p className="card-title">{photoBody}</p>}
          {email && (
            <p className="card-name">
              <a href={`mailto:${email}`}>{name}</a>
            </p>
          )}
          {username && <p className="card-username">@{username}</p>}
          {phone && <p className="card-phone">{phone}</p>}
        </div>
        <div className="card-body">
          {postBody && <p className="card-postBody">{postBody}</p>}
          {commentBody && <p className="card-postBody">{commentBody}</p>}
          {photoTitle && <p className="card-postBody">{photoTitle}</p>}
          {albumTitle && (
            <p className="card-postBody">
              <Link to={`${albumId}/photos`} onClick={onClick}>
                {albumTitle}
              </Link>
            </p>
          )}

          {city && (
            <>
              <p className="card-label">Address: </p>
              <p className="card-city">City: {city} </p>
              <p className="card-street">Street: {street} </p>
            </>
          )}
          {companyName && (
            <>
              <p className="card-label">Company</p>
              <p>Name: {companyName}</p>
              <p>{catchPhrase}</p>
            </>
          )}
        </div>
        {children && <div className="card-footer">{children}</div>}
        {commentEmail && (
          <div className="card-footer">
            <a href={`mailto:${commentEmail}`}>{commentEmail}</a>
          </div>
        )}
      </div>
    </div>
  )
}

export default Card
