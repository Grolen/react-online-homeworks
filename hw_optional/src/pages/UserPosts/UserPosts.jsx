import React, { useState, useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { useParams } from 'react-router-dom'
import Card from '../../components/Card'
import HeaderPage from '../../components/HeaderPage'
import Button from '../../components/Button'

import { sendRequest } from '../../helpers/sendRequest'
import { API_URL } from '../../config/API'
import { setPostName } from '../../store/actions'

import './UserPosts.scss'

const UserPosts = () => {
  const [posts, setPosts] = useState([])
  const entity = 'posts'
  const { UserID } = useParams()

  useEffect(() => {
    sendRequest(`${API_URL}/users/${UserID}/${entity}`).then((data) => {
      setPosts(data)
    })
  }, [UserID])

  const dispatch = useDispatch()

  const getPostName = (name) => {
    dispatch(setPostName(name))
  }

  const title = useSelector((state) => {
    const { userNameReducer } = state
    return `Posts - ${userNameReducer.userName}`
  })

  return (
    <>
      <HeaderPage title={title} />
      <div className="posts__dashboard">
        {posts &&
          posts.map(({ title, body, id, userId }, key) => (
            <Card postTitle={title} postBody={body} postId={id} key={key}>
              <Button
                isPrimary
                to={`${id}/comments`}
                onClick={() => getPostName(title)}
              >
                Comments
              </Button>
            </Card>
          ))}
      </div>
    </>
  )
}

export default UserPosts
