import { combineReducers } from '@reduxjs/toolkit'
import { userNameReducer, postNameReducer, albumNameReducer } from './reducers'

export const rootReducer = combineReducers({
  userNameReducer,
  postNameReducer,
  albumNameReducer,
})
