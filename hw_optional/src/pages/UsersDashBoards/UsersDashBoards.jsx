import React, { useState, useEffect } from 'react'
import { useDispatch } from 'react-redux'

import Button from '../../components/Button/Button'
import Card from '../../components/Card'
import HeaderPage from '../../components/HeaderPage'

import { sendRequest } from '../../helpers/sendRequest'
import { API_URL } from '../../config/API'
import { setUserName } from '../../store/actions'

import './UsersDashBoards.scss'

const UsersDashBoards = (props) => {
  const [users, setUsers] = useState([])
  const entity = 'users'
  useEffect(() => {
    sendRequest(`${API_URL}${entity}`).then((data) => {
      setUsers(data)
    })
  }, [])

  const dispatch = useDispatch()

  const getUserName = (name) => {
    dispatch(setUserName(name))
  }

  return (
    <>
      <HeaderPage title="Users" />
      <div className="users__dashboards">
        {users &&
          users.map(
            ({ id, name, username, email, address, company, phone }, key) => {
              const { city, street } = address
              const { name: companyName, catchPhrase } = company
              return (
                <Card
                  email={email}
                  name={name}
                  username={username}
                  phone={phone}
                  city={city}
                  street={street}
                  companyName={companyName}
                  catchPhrase={catchPhrase}
                  className="user__card"
                  key={key}
                >
                  <Button
                    isPrimary
                    to={`/user/${id}/posts`}
                    onClick={() => getUserName(name)}
                  >
                    Post
                  </Button>
                  <Button
                    isOutLineSecondary
                    to={`/user/${id}/todos`}
                    onClick={() => getUserName(name)}
                  >
                    Todos
                  </Button>
                  <Button
                    isOutLinePrimary
                    to={`/user/${id}/albums`}
                    onClick={() => getUserName(name)}
                  >
                    Albums
                  </Button>
                </Card>
              )
            }
          )}
      </div>
    </>
  )
}

export default UsersDashBoards
