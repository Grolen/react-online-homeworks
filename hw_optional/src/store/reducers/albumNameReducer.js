import { ACTION_ALBUMNAME } from '../types'
const initialState = {
  albumName: '',
}

export const albumNameReducer = (state = initialState, action) => {
  switch (action.type) {
    case ACTION_ALBUMNAME:
      return {
        ...state,
        albumName: action.payload,
      }
    default:
      return state
  }
}
