import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'
import { createBrowserHistory } from 'history'
import { useSelector } from 'react-redux'

import HeaderPage from '../../components/HeaderPage'
import Button from '../../components/Button'

import { sendRequest } from '../../helpers/sendRequest'
import { API_URL } from '../../config/API'

import './PostEdit.scss'

const PostEdit = () => {
  const [posts, setPosts] = useState([])
  const [post, setPost] = useState([])
  const entity = 'posts'
  const { UserID, PostID } = useParams()

  useEffect(() => {
    sendRequest(`${API_URL}/users/${UserID}/${entity}`).then((data) => {
      setPosts(data)
    })
  }, [UserID])

  useEffect(() => {
    const onePost = posts.find((post) => post.id === +PostID)
    setPost(onePost)
  }, [PostID, posts])

  const title = useSelector((state) => {
    const { userNameReducer, postNameReducer } = state
    return `Edit post - ${userNameReducer.userName} - ${postNameReducer.postName}`
  })

  const history = createBrowserHistory({ window })
  const clickBack = () => {
    history.back()
  }

  return (
    <>
      <HeaderPage title={title} />
      <div className="post__editor">
        {post && (
          <form>
            <div className="form-group">
              <label>Title</label>
              <input
                type="text"
                className="form-control"
                id="formGroupExampleInput"
                defaultValue={post.title}
              ></input>
            </div>
            <div className="form-group">
              <label>Body</label>
              <textarea
                type="text"
                className="form-control"
                id="formGroupExampleInput2"
                defaultValue={post.body}
                rows="6"
              ></textarea>
            </div>
            <Button isPrimary onClick={clickBack}>
              Update
            </Button>
          </form>
        )}
      </div>
    </>
  )
}

export default PostEdit
