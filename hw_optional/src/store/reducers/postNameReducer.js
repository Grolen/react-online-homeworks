import { ACTION_POSTNAME } from '../types'
const initialState = {
  postName: '',
}

export const postNameReducer = (state = initialState, action) => {
  switch (action.type) {
    case ACTION_POSTNAME:
      return {
        ...state,
        postName: action.payload,
      }
    default:
      return state
  }
}
