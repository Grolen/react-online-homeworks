import React from 'react'
import { Link } from 'react-router-dom'
import cx from 'classnames'
import PropTypes from 'prop-types'

const Button = (props) => {
  const {
    isPrimary,
    isSecondary,
    isOutLinePrimary,
    type,
    isOutLineSecondary,
    children,
    onClick,
    href,
    to,
    ...restProps
  } = props
  let Component = href ? 'a' : 'button'

  if (to) {
    Component = Link
  }

  return (
    <Component
      {...restProps}
      href={href}
      to={to}
      onClick={onClick}
      type={href || to ? undefined : type}
      className={cx(
        'btn',
        { 'btn-primary': isPrimary },
        { 'btn-secondary': isSecondary },
        { 'btn-outline-primary': isOutLinePrimary },
        { 'btn-outline-secondary': isOutLineSecondary }
      )}
    >
      {children}
    </Component>
  )
}

Button.propTypes = {
  isPrimary: PropTypes.bool,
  isSecondary: PropTypes.bool,
  isOutLinePrimary: PropTypes.bool,
  isOutLineSecondary: PropTypes.bool,
  type: PropTypes.string,
  onClick: PropTypes.func,
  href: PropTypes.string,
  to: PropTypes.string,
}

Button.defaultProps = {
  onClick: () => {},
  type: 'button',
}

export default Button
