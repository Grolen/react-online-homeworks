import { ACTION_USERNAME, ACTION_POSTNAME, ACTION_ALBUMNAME } from './types'

export const setUserName = (userName) => {
  return {
    type: ACTION_USERNAME,
    payload: userName,
  }
}
export const setPostName = (postName) => {
  return {
    type: ACTION_POSTNAME,
    payload: postName,
  }
}
export const setAlbumName = (albumName) => {
  return {
    type: ACTION_ALBUMNAME,
    payload: albumName,
  }
}
