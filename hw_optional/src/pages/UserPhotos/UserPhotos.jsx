import React, { useState, useEffect } from 'react'
import { useParams } from 'react-router-dom'
import { useSelector } from 'react-redux'

import Card from '../../components/Card'
import HeaderPage from '../../components/HeaderPage'

import { sendRequest } from '../../helpers/sendRequest'
import { API_URL } from '../../config/API'
import { ReactComponent as PhotoIcon } from './images/icon.svg'

import './UserPhotos.scss'

const UserPhotos = () => {
  const [photos, setPhotos] = useState([])
  const entity = 'photos'
  const { AlbumID } = useParams()

  useEffect(() => {
    sendRequest(`${API_URL}/albums/${AlbumID}/${entity}`).then((data) => {
      setPhotos(data)
    })
  }, [AlbumID])

  const title = useSelector((state) => {
    const { albumNameReducer, userNameReducer } = state
    return `Photos - ${userNameReducer.userName} - ${albumNameReducer.albumName}`
  })

  return (
    <>
      <HeaderPage title={title} />
      <div className="photos__dashboard">
        {photos &&
          photos.map(({ title }, key) => (
            <Card photoTitle={title} photoBody={<PhotoIcon />} key={key} />
          ))}
      </div>
    </>
  )
}

export default UserPhotos
