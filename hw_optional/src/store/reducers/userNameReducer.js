import { ACTION_USERNAME } from '../types'
const initialState = {
  userName: '',
}

export const userNameReducer = (state = initialState, action) => {
  switch (action.type) {
    case ACTION_USERNAME:
      return {
        ...state,
        userName: action.payload,
      }
    default:
      return state
  }
}
