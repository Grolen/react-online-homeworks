import React from 'react'

import ProductCard from '../ProductCard'
import './ProductList.scss'

const ProductList = ({
  items,
  addToFavourite,
  favouriteItems,
  onClickBtn,
  btnContent,
}) => {
  const isItemInFavourite = (item) => {
    return favouriteItems.find(
      (favouriteItem) => favouriteItem.articul === item.articul
    )
  }

  return (
    <div className="products">
      <div className="products__list">
        {items.map((item) => (
          <ProductCard
            key={item.articul}
            item={item}
            onClickStar={addToFavourite}
            onClickBtn={onClickBtn}
            isFavourite={isItemInFavourite(item)}
            btnContent={btnContent}
          />
        ))}
      </div>
    </div>
  )
}

export default ProductList
