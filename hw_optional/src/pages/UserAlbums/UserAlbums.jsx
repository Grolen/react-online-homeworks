import React, { useState, useEffect } from 'react'
import { useParams } from 'react-router-dom'
import { useSelector, useDispatch } from 'react-redux'

import Card from '../../components/Card'
import HeaderPage from '../../components/HeaderPage'

import { sendRequest } from '../../helpers/sendRequest'
import { API_URL } from '../../config/API'
import { ReactComponent as AlbumIcon } from './images/icon.svg'
import { setAlbumName } from '../../store/actions'

import './UserAlbums.scss'

const UserAlbums = () => {
  const [albums, setAlbums] = useState([])
  const entity = 'albums'
  const { UserID } = useParams()

  useEffect(() => {
    sendRequest(`${API_URL}/users/${UserID}/${entity}`).then((data) => {
      setAlbums(data)
    })
  }, [UserID])

  const dispatch = useDispatch()

  const getAlbumName = (name) => {
    dispatch(setAlbumName(name))
  }

  const title = useSelector((state) => {
    const { userNameReducer } = state
    return `Albums - ${userNameReducer.userName}`
  })

  return (
    <>
      <HeaderPage title={title} />
      <div className="albums__dashboard">
        {albums &&
          albums.map(({ title, id }, key) => (
            <Card
              albumTitle={title}
              albumBody={<AlbumIcon />}
              albumId={id}
              key={key}
              onClick={() => getAlbumName(title)}
            />
          ))}
      </div>
    </>
  )
}

export default UserAlbums
