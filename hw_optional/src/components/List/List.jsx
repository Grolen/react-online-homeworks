import React from 'react'

import './List.scss'

const List = ({ title, onClickFunc, index }) => {
  return (
    <li
      onClick={(element) => {
        onClickFunc(element, index)
      }}
      className="list-group-item"
    >
      {title}
    </li>
  )
}

export default List
