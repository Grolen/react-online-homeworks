import React, { useState, useEffect } from 'react'
import { useParams } from 'react-router-dom'
import { useSelector } from 'react-redux'

import HeaderPage from '../../components/HeaderPage'
import List from '../../components/List/List'

import { sendRequest } from '../../helpers/sendRequest'
import { API_URL } from '../../config/API'

import './UserTodos.scss'

const UserTodos = () => {
  const [todos, setTodos] = useState([])
  const entity = 'todos'
  const { UserID } = useParams()

  useEffect(() => {
    sendRequest(`${API_URL}/users/${UserID}/${entity}`).then((data) => {
      setTodos(data)
    })
  }, [UserID])

  const title = useSelector((state) => {
    const { userNameReducer } = state
    return `Todos - ${userNameReducer.userName}`
  })

  const onClickTodo = (todo, index) => {
    todo.target.classList.toggle('done')
    const newTodoList = [...todos]
    newTodoList[index].completed = !newTodoList[index].completed
    setTodos(newTodoList)
  }
  return (
    <>
      <HeaderPage title={title} />
      <div className="todos__dashboard">
        <ul className="list-group">
          {todos &&
            todos.map(({ title }, key) => (
              <List
                title={title}
                onClickFunc={onClickTodo}
                key={key}
                index={key}
              />
            ))}
        </ul>
      </div>
    </>
  )
}

export default UserTodos
