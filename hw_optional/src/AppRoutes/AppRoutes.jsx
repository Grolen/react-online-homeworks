import React from 'react'
import { Routes, Route } from 'react-router-dom'

import UsersDashBoards from '../pages/UsersDashBoards'
import UserPosts from '../pages/UserPosts'
import UserAlbums from '../pages/UserAlbums'
import UserPhotos from '../pages/UserPhotos'
import UserTodos from '../pages/UserTodos'
import PostComments from '../pages/PostComments'
import PostEdit from '../pages/PostEdit'

const AppRoutes = () => {
  return (
    <Routes>
      <Route index element={<UsersDashBoards />} />
      <Route path={`/user/:UserID/posts/`} element={<UserPosts />} />
      <Route path={`/user/:UserID/todos/`} element={<UserTodos />} />
      <Route path={`/user/:UserID/albums/`} element={<UserAlbums />} />
      <Route
        path={`/user/:UserID/albums/:AlbumID/photos`}
        element={<UserPhotos />}
      />
      <Route
        path={`/user/:UserID/posts/:PostID/comments`}
        element={<PostComments />}
      />
      <Route path={`/user/:UserID/posts/:PostID/edit`} element={<PostEdit />} />
    </Routes>
  )
}

export default AppRoutes
