import { userNameReducer } from './userNameReducer'
import { postNameReducer } from './postNameReducer'
import { albumNameReducer } from './albumNameReducer'

export { userNameReducer, postNameReducer, albumNameReducer }
