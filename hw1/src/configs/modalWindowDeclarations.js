const modalWindowDeclarations = [
  {
    id: 'modalId1',
    header: 'Modal Window 1',
    closeButton: true,
    text: 'Is this a first modal window with a closeButton?',
  },
  {
    id: 'modalId2',
    header: 'Modal Window 2',
    closeButton: false,
    text: 'Is this a second modal window without a closeButton?',
  },
]
export default modalWindowDeclarations
